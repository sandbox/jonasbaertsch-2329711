Bootstrap Accessibility Library for Drupal

This module loads Bootstrap accessibility files, regardless of the theme you are 
using. It does nothing else.

When using Bootstrap themes is particularly helpful to correct Bootstrap 
accessibility issues.

This module does not depend on a specific version of Bootstrap, it just loads 
the version you install, with the components you install on it, either complete 
or customize.

Installation

- Download Boostrap files on /sites/all/libraries/accessibility.
- Install and enable Boostrap accessibility library module.
- Configure on which pages you want to load the libraries (by default everything 
  but admin/*).
